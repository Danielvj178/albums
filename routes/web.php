<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('inicio', compact('albumsUser', 'users', 'info'));
});*/

Route::get('/', function () {
    return view('login');
});

/** Definición de rutas */
Route::resource('/', 'LoginController');
//Route::resource('inicio','InicioController');
// Route::resource('login','LoginController');
Route::resource('share/saveshare','ShareController');

//Route::resource('inicio', 'InicioController@index');
Route::get('inicio', [
    'as' => 'inicio',
    'uses' => 'InicioController@store',
]);
Route::get('album/{idalbum}', [
    'as' => 'album',
    'uses' => 'AlbumController@index',
]);
Route::get('comments/{idpost}', [
    'as' => 'comments',
    'uses' => 'CommentsController@index',
]);
Route::get('share/{idalbum}', [
    'as' => 'share',
    'uses' => 'ShareController@index',
]);


/** Rutas para Ajax */
Route::post('consultUsers', 'ShareController@consultUsers');
Route::post('consultPermissions', 'AlbumController@consultPermissions');
Route::post('consultComments', 'CommentsController@consultComments');
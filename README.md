# Prueba técnica álbumes

El proyecto consiste en realizar la invocación de unas API'S para obtener información
acerca de álbumes, fotos, posts, comentarios y usuarios

## Explicación de estructura del proyecto 🚀

*  Controladores

En los controladores se encuentra desarrollada toda la parte de la lógica de la aplicación.
La ruta en dónde se podrán visualizar los controladores
```
app/Http/Controllers
```

* Vistas

En las vistas es en dónde se visualizan todos los datos devueltos, y la lógica a visualizar por pantalla
generada en los controladores.
La ruta en dónde se podrán visualizar las vistas
```
resources/views
```

* Modelo

En el modelo es en dónde se persisten los datos que se guardarán en la bases de datos
La ruta en dónde se podrán visualizar los modelos
```
app/
```

* Base de datos

Se utilizó una tabla para guardar los permisos y los álbumes compartidos con los demás usuarios de la plataforma
La ruta en dónde se encuentra el script de la base de datos
```
scriptDB
```
## Construido con 🛠️

*  PHP - Laravel 5.6
*  MySQL

### Ejecución del proyecto en entorno local 🔧

Una vez realizada la clonación del proyecto, se debe de proceder a
ingresar a la carpeta del proyecto por medio de la consola
y se deberán ejecutar los siguientes comandos

```
composer install
```

Una vez instaladas las dependencias debería de ingresar a la ruta principal y visualizarse el loguin
```
/albums/public/
```

En caso de que se visualice un error 500, se deberá de ejecutar el siguiente comando
```
php artisan key:generate
```

En el archivo .env se debe de colocar una clave única que genera este comando para otorgar los permisos a la aplicación
En este archivo se encuentra toda la configuración de base de datos

## Autores ✒️

* **Daniel Vidal Jaramillo** - *Prueba técnica - álbumes*

## Licencia 📄

Este proyecto está bajo la Licencia (MIT License)

## Agradecimientos 🎁

* Gracias por su atención 🍺 .
<?php

namespace App\Http\Controllers;

use App\Share;
use Illuminate\Http\Request;

class ShareController extends Controller
{
    /**
        Función para visualizar el módulo de compartimiento
    */
    public function index($idalbum){
        return view('share', compact('idalbum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Definición de variables
        $data = $request->all();
        $data = $request->except(['_token']);

        $this->validaciones($request);

        $share = Share::forceCreate($data);

        return redirect()->route('share', $data['album_id'])->with('flash_message', 'Álbum compartido correctamente');
    }

    /**
        Función para realizar validaciones en el formulario
    */
    public function validaciones($request){
        $rules = [
            'permission' => 'required',
            'user_permission'    => 'required'
        ];

        $messages = [
            'permission.required' => 'Por favor seleccione el permiso del álbum',
            'user_permission.required' => 'Por favor seleccione el usuario a compartir el álbum',
        ];
         
        $this->validate($request, $rules, $messages);
    }

    /** 
        Función para obtener usuarios a compartir el álbum
    */
    public function consultUsers(){
        $user = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users"), true);

        echo json_encode($user);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use Session;
use App\Share;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){
    	//Declaración de variables
    	$data = array();
    	$dataShare = array();

    	session([
    		'email' => $request->input('email'),
    		'username' => $request->input('username'),
    	]);

        // Verificación de usuario en API 
        $user = $this->searchUser(session('email'), session('username'));

        if( $user ) {
            /** Obtención de álbumes por usuario */
            $albumsUser = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/albums?userId={$user[0]['id']}"),true);

            /** Obtención de mis posts */
            $posts = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/posts?userId={$user[0]['id']}"),true);

            /**Obtención de fotos del sistema */ 
            $photos = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/photos?albumId={$albumsUser[0]['id']}"), true);

            /** Obtención de usuario por albúm */
            $users = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users?id={$albumsUser[0]['userId']}"), true);

            $albumsShare = $this->searchShare(session('username'));

			//Información a visualizar en la vista para la carga de álbumes
            foreach ($albumsUser as $key => $value) {
                $data[$key]['albumId'] = $value['id'];
                $data[$key]['albumTitle'] = $value['title'];
                $data[$key]['name'] = $users[0]['name'];
                $data[$key]['username'] = $users[0]['username'];
                $data[$key]['url'] = $photos[0]['thumbnailUrl'];
            }

            //Información de álbumes compartidos
            foreach ($albumsShare as $key => $value) {
                $posData = array_search($value['album_id'], array_column($data, 'albumId'));
                $dataShare[$key] = $data[$posData];
                $dataShare[$key]['permission'] = $value['permission'];
            }

            return view('inicio', compact('data', 'posts', 'dataShare'));
        } else {
            Session::flash('flash_message', 'El usuario no ha sido encontrado');
            return view('login');
        }
    }

    /**
        Función para búsqueda de usuario en el API
    */
    public function searchUser($email, $username){
        $user = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users?username=$username&email=$email"), true);

        return $user;
    }

    /** 
        Función para consultar los albumes compartidos con el usuario
    */    
    public function searchShare($username){
    	//Declaración de variables
    	$dataShare = array();

        $album = Share::whereRaw("user_permission = '".$username."'")->get()->toArray();

        foreach ($album as $key => $value) {
            $dataShare[$key]['album_id'] = $value['album_id'];
            $dataShare[$key]['permission'] = $value['permission'];
        }

        return $dataShare;
    }
}

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Share;

class AlbumController extends Controller
{

    public function index($idalbum){
        /** Obtención de fotos por album */
        $photos = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/photos?albumId=$idalbum"), true);

        $users = $this->searchUsersAlbum($idalbum);

        return view('album', compact('photos', 'users'));
    }

    /** 
        Función para consultar los álbumes compartidos con el usuario
    */    
    public function searchUsersAlbum($idalbum){
        //Definición de variables
        $data = array();

        $album = Share::whereRaw("album_id = '".$idalbum."'")->get()->toArray();

        foreach ($album as $key => $value) {
            $data[$key]['user_permission'] = $value['user_permission'];
            $data[$key]['permission'] = $value['permission'];
        }

        return $data;
    }

    /**
    	Función para búsqueda de usuarios por permisos
    	de cada álbum
    */
    public function consultPermissions(){
    	$albumId = $_POST['album_id'];
		$permission = $_POST['permission'];

		$users = Share::whereRaw("permission = '".$permission."' and album_id = '".$albumId."'")->get()->toArray();
    	
    	echo json_encode($users);
    } 
}
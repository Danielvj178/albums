<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CommentsController extends Controller
{
	/** 
        Función para obtener los comentarios del post seleccionado 
	*/
    public function index($idPost){
        //Declaración de variables
        $userComments = array();

        $comments = $this->getComments($idPost);

        //Usuarios que realizaron comentarios en el post
        foreach ($comments as $key => $value) {
            $userComments[$value['email']] = $value['email'];
        }

        return view('comments', compact('comments', 'userComments'));
    }

    /** 
        Función para consultar los comentarios
    */
    public function getComments($idPost=null, $email=null, $name=null){
        if( $idPost )
            $params = 'postId='.$idPost;
        else if( $email )
            $params = 'email='.$email;
        else
            $params = 'name='.urlencode($name);

        /** Obtención de comentarios por Post */
        $comments = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/comments?$params"), true);

        return $comments;
    }

    /**
        Función para filtro de comentarios
    */
    public function consultComments(){
        //Definición de variables
        $user = $_POST['user'];
        $name = $_POST['name'];
        $allComments = $_POST['allComments'];

        if( $allComments )
            $comments = $this->getComments($allComments);
        else if( $user )
            $comments = $this->getComments(null,$user);
        else if( $name )
            $comments = $this->getComments(null, null, $name);

        echo json_encode($comments);
    }
}
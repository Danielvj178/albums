<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Share;

class InicioController extends Controller
{

    public function index(){

    	$this->a();
    }

    public function store(Request $request){
    	//Declaración de variables
    	$data = array();
    	$albumsShare = array();

    	if( $request->input('email') ){
    		session([
    			'email' => $request->input('email'),
    			'username' => $request->input('username'),
    		]);
    	} 

        // Verificación de usuario en API 
        $user = $this->searchUser(session('email'), session('username'));

        if( $user ) {
            /** Obtención de álbumes por usuario */
            $albumsUser = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/albums?userId={$user[0]['id']}"),true);

            /** Obtención de mis posts */
            $posts = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/posts?userId={$user[0]['id']}"),true);

        	$photos = $this->searchPhoto($albumsUser[0]['id']);

        	$users = $this->searchUserAlbum($albumsUser[0]['userId']);

            $albumsShare = $this->searchShare(session('username'));

			//Información a visualizar en la vista para la carga de álbumes
            foreach ($albumsUser as $key => $value) {
                $data[$key]['albumId'] = $value['id'];
                $data[$key]['albumTitle'] = $value['title'];
                $data[$key]['name'] = $users[0]['name'];
                $data[$key]['username'] = $users[0]['username'];
                $data[$key]['url'] = $photos[0]['thumbnailUrl'];
            }

            return view('inicio', compact('data', 'posts', 'albumsShare'));
        } else {
            Session::flash('flash_message', 'El usuario no ha sido encontrado');
            return view('login');
        }
    }

    /**
        Función para búsqueda de usuario en el API
    */
    public function searchUser($email, $username){
        $user = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users?username=$username&email=$email"), true);

        return $user;
    }

    /**
		Función para búsqueda de úsuario por álbum
    */
	public function searchUserAlbum($idUserAlbum){
		/** Obtención de usuario por albúm */
        $users = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/users?id=$idUserAlbum"), true);

        return $users;
	}

    /**
		Función para búsquedad de foto de portada del álbum
    */
	public function searchPhoto($idAlbum){
		/**Obtención de fotos del sistema */ 
		$photos = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/photos?albumId=$idAlbum"), true);

		return $photos;
	}

	/**
		Función para búsqueda de álbum
	*/
	public function searchAlbum($idAlbum){
		$album = json_decode(file_get_contents("https://jsonplaceholder.typicode.com/albums?id=$idAlbum"), true);

		return $album;
	}

    /** 
        Función para consultar los albumes compartidos con el usuario
    */    
    public function searchShare($username){
    	//Declaración de variables
    	$dataShare = array();

        $album = Share::whereRaw("user_permission = '".$username."'")->get()->toArray();

        foreach ($album as $key => $value) {
            $albumShare = $this->searchAlbum($value['album_id']);
            $dataShare[$key]['albumId'] = $albumShare[0]['id'];
            $dataShare[$key]['albumTitle'] = $albumShare[0]['title'];

            // Consulta datos API 
            $users = $this->searchUserAlbum($albumShare[0]['userId']);
            $photos = $this->searchPhoto($value['album_id']);

            $dataShare[$key]['name'] = $users[0]['name'];
            $dataShare[$key]['username'] = $users[0]['username'];
            $dataShare[$key]['url'] = $photos[0]['thumbnailUrl'];
            $dataShare[$key]['permission'] = $value['permission'];
        }

        return $dataShare;
    }
}
@extends('layouts.base')

<link href="../css/comments.css" rel="stylesheet">
@section('title')
	Comments
@endsection

@section('message_banner')
    Comentarios del Post #{{ $comments[0]['postId'] }}
@endsection

@section('description_banner')
    Sistema para interacción de álbumes de fotos
@endsection

@section('content')
	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token"/>
	{{-- Bloque de filtro de búsqueda por comentarios --}}
	<div class="alert alert-info" style="text-align: center;">
		Búsqueda por comentarios 
	</div>

	<div class="row" style="margin-left: 25%">
		<div class="col col-md-3">
			<label>Usuario</label>
		</div>
		<div class="col col-md-4">
			@if( count($userComments) > 0 )
				<select id="user" class="form-control" name="user">
					<option value="">Seleccione</option>
					@foreach($userComments as $key => $value)
						<option value="{{ $value }}">{{ $value }}</option>
					@endforeach
					<option value="{{ $comments[0]['postId'] }}">Todos</option>
				</select>
			@else
				<div class="alert alert-danger" style="text-align: center;">
					Ningún usuario ha comentado el Post
				</div>
			@endif
		</div>
	</div>
	<hr>
	<div class="row" style="margin-left: 25%">
		<div class="col col-md-3">
			<label>Nombre comentario</label>
		</div>
		<div class="col col-md-4">
			<input type="text" class="form-control" name="name" id="nameComment" />
		</div>
	</div>
	<hr>
	<div class="justify-content-md-center" style="text-align: center;">
		<button id="btnBuscar" class="btn btn-sm btn-outline-success">Buscar</button>
	</div>

	{{-- Visualización de comentarios --}}
	@for($i=0; $i < count($comments); $i++)
		<div class="comments-container">
			<ul id="comments-list" class="comments-list">
				<li>
					<div class="comment-main-level">
						<!-- Avatar -->
						<div class="comment-avatar"><img src="https://www.coordinadora.com/wp-content/uploads/sidebar_usuario-corporativo.png" alt=""></div>
						<!-- Contenedor del Comentario -->
						<div class="comment-box">
							<div class="comment-head">
								<h6 class="comment-name by-author"><a href="http://creaticode.com/blog">{{ $comments[$i]['email'] }}</a></h6>
							</div>
							<div class="comment-content">
								<h6>Nombre</h6>
								{{ $comments[$i]['name'] }}
							</div>
							<div class="comment-content">
								<h6>Contenido</h6>
								{{ $comments[$i]['body'] }}
							</div>
						</div>
					</div>
				</li>
			</ul>
		</div>
	@endfor
@endsection
@section('scripts')
	<script src="../js/comments.js"></script>
@endsection
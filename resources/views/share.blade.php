@extends('layouts.base')

@section('title')
	Compartir álbum
@endsection

@section('message_banner')
    Compartir álbum #{{$idalbum}}
@endsection

@section('description_banner')
    Módulo de compartimiento de álbumes
@endsection

@section('content')
	<form action="saveshare" method="post">
		@csrf

		{{-- Mensaje de estado de la acción --}}
		@if(Session::has('flash_message'))
		  <div class="alert alert-success" style="text-align: center;">{{Session::get('flash_message')}}</div>
		@endif

		{{-- Mensaje para validaciones --}}
		@if (count($errors) > 0)
		    <div class="alert alert-danger">
		        <p>Corrige los siguientes errores:</p>
		        <ul>
		            @foreach ($errors->all() as $message)
		                <li>{{ $message }}</li>
		            @endforeach
		        </ul>
		    </div>
		@endif

		{{-- Formulario para control de permisos --}}
		<div class="alert alert-info" style="text-align: center;">
			Compartir álbum
		</div>
		<div class="form-group">
			<input type="hidden" name="album_id" value="{{$idalbum}}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
			<div class="row" style="margin-left: 25%">
				<div class="col col-md-4">
					<label>Permiso del álbum</label>
				</div>
				<div class="col col-md-4">
					<select id="permisos" class="form-control" name="permission">
						<option value="">Seleccione</option>
						<option value="lectura">Lectura</option>
						<option value="escritura">Escritura</option>
						<option value="lectura y escritura">Lectura y escritura</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row" style="margin-left: 25%">
				<div class="col col-md-4">
					<label>Usuario a asignar permiso</label>
				</div>
				<div class="col col-md-4">
					<select id="selectUsuario" class="form-control" name="user_permission">
					</select>
				</div>
			</div>
			<hr>
			<div style="text-align: center;">
			    <button type="submit" class="btn btn-sm btn-outline-success">Compartir álbum</button>
			</div>
		</div>
	</form>
@endsection

@section('scripts')
	<script src="../js/share.js"></script>
@endsection
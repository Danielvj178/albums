@extends('layouts.base')

@section('title')
    Inicio
@endsection

@section('message_banner')
    Bienvenido al sistema de álbumes Sr@ {{ $data[0]['name'] }}
@endsection

@section('description_banner')
    Sistema para interacción de álbumes de fotos
@endsection

@section('content')

    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="margin-left: 36%">
      <li class="nav-item">
        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Mis álbumes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Mis posts</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Mis álbumes compartidos</a>
      </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
      <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
        <form class="form-group" id="form-inicio" method="POST" action="album" >
            @csrf
            <div class="row">
            @for($i=0; $i<count($data); $i++)
                <div class="col-md-4">
                  <div class="card mb-4 shadow-sm">
                    <img src="{{ $data[$i]['url'] }}" width="100%" height="225" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Albúm {{ $data[$i]['albumId'] }}</h5>
                        <h6>Descripción albúm</h6>
                      <p class="card-text">
                        {{ $data[$i]['albumTitle'] }}
                        </p>
                        <h6>Autor</h6>
                        <p class="card-text">
                            {{ $data[$i]['username'] }}
                            {{ $data[$i]['name'] }}
                        </p>
                        
                      <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                          <a class="btn btn-sm btn-outline-secondary" href="{{url('album', ['idalbum' => $data[$i]['albumId']])}}">Ver albúm de fotos</a>
                          <a class="btn btn-sm btn-outline-secondary" href="{{url('share', ['idalbum' => $data[$i]['albumId']])}}">Compartir albúm</a>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
            @endfor
            </div>
        </form>
      </div>
      <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
          <form class="form-group" id="form-inicio" method="POST" action="comments" >
              @csrf
              <div class="row">
              @for($i=0; $i<count($posts); $i++)
                  <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                      <div class="card-body">
                          <h5 class="card-title">Post #{{ $posts[$i]['id'] }} - {{ $posts[$i]['title'] }}</h5>
                          <h6>Descripción del Post</h6>
                        <p class="card-text">{{ $posts[$i]['body'] }}</p>
                        <div class="d-flex justify-content-between align-items-center">
                          <div class="btn-group">
                            <a class="btn btn-sm btn-outline-secondary" href="{{url('comments', ['idPost' => $posts[$i]['id']])}}">Ver comentarios</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
              @endfor
              </div>
          </form>
      </div>
      <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
        <form class="form-group" id="form-inicio" method="POST" action="album" >
            @csrf
            <div class="row">
              @if( count($albumsShare) > 0 ) 
                @for($i=0; $i<count($albumsShare); $i++)
                    <div class="col-md-4">
                      <div class="card mb-4 shadow-sm">
                        <img src="{{ $albumsShare[$i]['url'] }}" width="100%" height="225" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">Albúm {{ $albumsShare[$i]['albumId'] }}</h5>
                            <h6>Descripción albúm</h6>
                          <p class="card-text">
                            {{ $albumsShare[$i]['albumTitle'] }}
                            </p>
                            <h6>Autor</h6>
                            <p class="card-text">
                                {{ $albumsShare[$i]['username'] }}
                                {{ $albumsShare[$i]['name'] }}
                            </p>
                            
                          <div class="d-flex justify-content-between align-items-center">
                            <div class="btn-group">
                              @if( $albumsShare[$i]['permission'] == 'lectura' )
                                <a class="btn btn-sm btn-outline-secondary" href="{{url('album', ['idalbum' => $albumsShare[$i]['albumId']])}}">Ver albúm de fotos</a>
                              @elseif( $albumsShare[$i]['permission'] == 'escritura' )
                                <a class="btn btn-sm btn-outline-secondary" href="{{url('share', ['idalbum' => $albumsShare[$i]['albumId']])}}">Compartir albúm</a>
                              @else
                                <a class="btn btn-sm btn-outline-secondary" href="{{url('album', ['idalbum' => $albumsShare[$i]['albumId']])}}">Ver albúm de fotos</a>
                                <a class="btn btn-sm btn-outline-secondary" href="{{url('share', ['idalbum' => $albumsShare[$i]['albumId']])}}">Compartir albúm</a>
                              @endif
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </div>
                @endfor
                @else
                  <div class="col col-md-12 alert alert-danger" style="text-align: center;">
                    Usted no cuenta con álbumes compartidos
                  </div>
                @endif
              </div>
          </form>
      </div>
    </div>
@endsection

@extends('layouts.base')

@section('title')
	Fotos de álbumes
@endsection

@section('message_banner')
	Álbum de fotos #{{ $photos[0]['albumId'] }} seleccionado
@endsection

@section('description_banner')
	Ver todas las fotos del álbum
@endsection

@section('content')
	<input type="hidden" name="_token" value="{{ csrf_token() }}" id="token"/>
	<input type="hidden" id="album_id" value="{{ $photos[0]['albumId'] }}"/>
	<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist" style="margin-left: 36%">
	  <li class="nav-item">
	    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Fotos del álbum</a>
	  </li>
	  <li class="nav-item">
	    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Compartido con</a>
	  </li>
	</ul>
	<div class="tab-content" id="pills-tabContent">
	  <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
	    	<form class="form-group" id="form-inicio" method="POST" action="" >
	        	@csrf
	    		<div class="row">
		    		@for($i=0; $i<count($photos); $i++)
		    			<div class="col-md-4">
		    			  <div class="card mb-4 shadow-sm">
		    			  	<img src="{{ $photos[$i]['thumbnailUrl'] }}" width="100%" height="225" class="card-img-top" alt="...">
		    			    <div class="card-body">
		    			    	<h5 class="card-title">Foto #{{ $photos[$i]['id'] }}</h5>
		    			    	<h6>Titulo de la foto</h6>
		    			      <p class="card-text">{{ $photos[$i]['title'] }}</p>
		    			    </div>
		    			  </div>
		    			</div>
		    		@endfor
	    		</div>
	    	</form>
	  </div>
	  <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
	  		<div class="row" style="margin-left: 25%">
				<div class="col col-md-3">
					<label>Permiso del álbum</label>
				</div>
				<div class="col col-md-3">
					<select id="permisos" class="form-control" name="permission">
						<option value="">Seleccione</option>
						<option value="lectura">Lectura</option>
						<option value="escritura">Escritura</option>
						<option value="lectura y escritura">Lectura y escritura</option>
					</select>
				</div>
			</div>
			<hr>
			<div class="row justify-content-md-center">
				<button id="btnBuscar" class="btn btn-sm btn-outline-success">Buscar</button>
			</div>
			<br>
	      @if( count($users) > 0 ) 
	      		<div class="col-md-12">
	      		  <table class="table table-dark">
	      		      <thead>
	      		          <th style="text-align: center;">Usuario</th>
	      		          <th style="text-align: center;">Permiso</th>
	      		      </thead>
	      		      <tbody style="text-align: center;" id="tblUsers">
	      		      	@for($i=0; $i<count($users); $i++)
		      		      	<tr>
		      		      		<td>{{ $users[$i]['user_permission'] }}</td>
		      		      		<td>{{ $users[$i]['permission'] }}</td>
		      		      	</tr>
		      		    @endfor
	      		      </tbody>
	      		  </table>
	      		</div>
	      @else
	      	<div class="alert alert-danger" style="text-align: center;">
	      	  Este álbum no ha sido compartido
	      	</div>
	      @endif
	  </div>
	</div>
@endsection

@section('scripts')
	<script src="../js/album.js"></script>
@endsection
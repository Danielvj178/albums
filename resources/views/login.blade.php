@extends('layouts.base-login')

@section('title', 'Portal de Álbumes')

@section('content')
    <div class="container">
	   <div class="card card-login mx-auto mt-5 ">
        <div class="card-header" style="background-color: #1c734c;color: white;">Ingreso a portal</div>
        <div class="card-body">
          <form method="get" action="inicio">
            <!-- Protección de ataques peligrosos -->
			@csrf

      {{-- Mensaje de estado de la acción --}}
        @if(Session::has('flash_message'))
          <div class="alert alert-danger" style="text-align: center;">{{Session::get('flash_message')}}</div>
        @endif
			<div class="form-group">
              <div class="form-label-group">
                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Correo electrónico" required="required" autofocus="autofocus">
                <label for="inputEmail">Correo electrónico</label>
              </div>
            </div>
            <div class="form-group">
              <div class="form-label-group">
                <input type="text" id="inputUsername"  class="form-control" name="username" placeholder="Usuario" required="required">
                <label for="inputUsername">Usuario</label>
              </div>
            </div>
			<button type="submit" class="btn btn-personalizado btn-block">Ingresar</button>
          </form>
        </div>
        <script type="text/javascript">
            $("body").addClass('bg-dark');
        </script>
      </div>
    </div>
      	
@endsection
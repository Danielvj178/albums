$(document).ready(function (){
    consultUsers();

    /* Método para cargar desplegable con los usuarios  */
    function consultUsers(){
        $("#selectUsuario").empty();
        $("#selectUsuario").html();
        url = "http://localhost/albums/public/consultUsers";
        token = $("#token").val();
        $.ajax({
            url: url,
            type: 'POST',
            headers: {'X-CSRF-TOKEN': token},
            dataType: 'json',
            success: function(result){
                $("#selectUsuario").append('<option value>Seleccione...</option>');
                $(result).each(function (i, v){
                    $("#selectUsuario").append('<option value="'+v.username+'">'+v.name+'</option>');
                });
            }, error: function(e){
            	alert(e);
            }
        });
    }
});
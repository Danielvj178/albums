$(document).ready(function (){
	token = $("#token").val();
	albumId = $("#album_id").val();
	$("#btnBuscar").click(function (e){
		permission = $("#permisos").val();
		url = "http://localhost/albums/public/consultPermissions";
	    $("#tblUsers").empty();
	    $("#tblUsers").html();
		e.preventDefault();
	    $.ajax({
	        url: url,
	        type: 'POST',
	        headers: {'X-CSRF-TOKEN': token},
	        dataType: 'json',
	        data:{
	        	album_id: albumId,
	        	permission: permission
	        },
	        success: function(result){
	            $(result).each(function (i, v){
                    $("#tblUsers").append("<tr>"
      		      		+"<td>"+v.user_permission+"</td>"
      		      		+"<td>"+v.permission+"</td>"
      		      	+"</tr>");
                });
	        },
	        error: function(e){
	        	console.log(e);
	        }
	    });
	});
	});

$(document).ready(function (){
	token = $("#token").val();
	
	$("#btnBuscar").click(function (e){
		user = $("#user").val();
		name = $("#nameComment").val();
		allComments = null;

		if( $("#user option:selected").text() == 'Todos' )
			allComments = user;
		
		url = "http://localhost/albums/public/consultComments";
	    $(".comments-list").empty();
	    $(".comments-list").html();
		e.preventDefault();
	    $.ajax({
	        url: url,
	        type: 'POST',
	        headers: {'X-CSRF-TOKEN': token},
	        dataType: 'json',
	        data:{
	        	user: user,
	        	name: name,
	        	allComments: allComments
	        },
	        success: function(result){
	        	console.log(result);
	            $(result).each(function (i, v){
	            	console.log(i+' '+v);
                    $("#comments-list").append('<li>'
						+'<div class="comment-main-level">'
							+'<div class="comment-avatar"><img src="https://www.coordinadora.com/wp-content/uploads/sidebar_usuario-corporativo.png" alt=""></div>'
							+'<div class="comment-box">'
								+'<div class="comment-head">'
									+'<h6 class="comment-name by-author"><a href="http://creaticode.com/blog">'+v.email+'</a></h6>'
								+'</div>'
								+'<div class="comment-content">'
									+'<h6>Nombre</h6>'
									+v.name
								+'</div>'
								+'<div class="comment-content">'
									+'<h6>Contenido</h6>'
									+v.body
								+'</div>'
							+'</div>'
						+'</div>'
					+'</li>');
                });
	        },
	        error: function(e){
	        	console.log(e);
	        }
	    });
	});
});